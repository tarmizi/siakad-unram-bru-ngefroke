<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Nilai' )
    ->set_page_name( 'Nilai' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$is_peserta = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'peserta' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_peserta ) : ?>
                <h1 class="page-header">
                    Peserta
                    <small>MK123</small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-2">
                            <select class="form-control">
                                <option>--tahun ajaran</option>
                                <option>2010</option>
                                <option>2011</option>
                                <option>2012</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                        <th>Nilai</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range( 1, 3 ) as $i ) : ?>
                        <tr>
                            <td>F1B008004</td>
                            <td>Ahmad Zafrullah</td>
                            <td>8</td>
                            <td>A</td>
                            <td><input type="text" value="A"></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-ok"></i> Konfirmasi</button>
            <?php else : ?>
                <h1 class="page-header">
                    Nilai
                    <small>Mata Kuliah</small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>--bidang keahlian</option>
                                <option>Sistem Cerdas</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Kode Mata Kuliah</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range( 1, 3 ) as $i ) : ?>
                        <tr>
                            <td><a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $i; ?>/peserta">MK123</a></td>
                            <td>Bahasa Sasak</td>
                            <td>8</td>
                            <td>A</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <p>Entry nilai secara masal dengan meng-unggah file <strong>.csv</strong> dengan <a href="#">format ini</a></p>
                <div class="form-group">
                    <div class="col-sm-3">
                        <input type="file">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-cloud-upload"></i> Unggah</button>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();