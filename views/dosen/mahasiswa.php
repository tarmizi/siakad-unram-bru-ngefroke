<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Mahasiswa Bimbingan' )
    ->set_page_name( 'Mahasiswa' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$is_detail = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'detil' );
$is_krs = Routes::get_instance()->has_tingkat( 3 ) && Routes::get_instance()->is_tingkat( 4, 'krs' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_krs ) : ?>
                <h1 class="page-header">
                    F1B008004
                    <small>Pengajuan KRS</small>
                </h1>
                <div class="alert alert-warning">
                    <strong>Peringatan :</strong> side sudah semester 8, tapi sks masih 123.
                </div>
                <table style="width: 100%">
                    <thead>
                    <tr>
                        <th>Tahun Ajaran</th>
                        <td>2014</td>
                    </tr><tr>
                        <th>Semester</th>
                        <td>8 / Genap</td>
                    </tr><tr>
                        <th>SKS yang telah diambil</th>
                        <td>123</td>
                    </tr><tr>
                        <th>SKS yang boleh diambil</th>
                        <td>24</td>
                    </tr>
                    </thead>
                </table>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Kode</th>
                        <th>Nama</th>
                        <th>SKS</th>
                        <th>Prasyarat</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>MK12</td>
                        <td>Bahasa Sasak</td>
                        <td>4</td>
                        <td>
                            <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Prancis)</span><br/>
                            <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> MK111 (Bahasa Jerman)</span><br/>
                        </td>
                    </tr><tr>
                        <td>MK12</td>
                        <td>Bahasa India</td>
                        <td>4</td>
                        <td>
                            <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Inggris)</span><br/>
                            <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Arab)</span><br/>
                        </td>
                    </tr>
                    </tbody>
                </table>
                <div class="form-group">
                    <label class="col-md-1 control-label">Pesan</label>
                    <div class="col-md-3">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
                <div class="clearfix"></div>
                <br/>

                <button class="btn btn-sm btn-success"><i class="glyphicon glyphicon-ok"></i> Terima</button>
                <button class="btn btn-sm btn-danger"><i class="glyphicon glyphicon-remove"></i> Tolak</button>
            <?php elseif( $is_detail ) : ?>
                <h1 class="page-header">
                    F1B008004
                    <small>Ahmad Zafrullah</small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>--semester</option>
                                <option>1</option>
                                <option>2</option>
                                <option selected>3</option>
                                <option>4</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>Kode Mata Kuliah</th>
                        <th>Nama Mata Kuliah</th>
                        <th>Semester</th>
                        <th>Kelas</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range( 1, 3 ) as $i ) : ?>
                        <tr>
                            <td>MK123</td>
                            <td>Bahasa Sasak</td>
                            <td>8</td>
                            <td>A</td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else : ?>
                <h1 class="page-header">
                    Mahasiswa
                    <small>Bimbingan Akademik</small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-3">
                            <select class="form-control">
                                <option>--bidang keahlian</option>
                                <option>Sistem Cerdas</option>
                            </select>
                        </div>
                        <div class="col-sm-3">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>NIM</th>
                        <th>Nama</th>
                        <th>Bidang Keahlian</th>
                        <th>Semester</th>
                        <th>Keterangan</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range( 1, 3 ) as $i ) : ?>
                        <tr>
                            <td>F1B008004</td>
                            <td>Ahmad Zafrullah</td>
                            <td>Teknik Informatika</td>
                            <td>14</td>
                            <td><span class="text-danger"><strong>Peringatan : </strong> mahasiswa ini terancam DO.</span></td>
                            <td>
                                <a class="btn btn-sm btn-primary" title="Detil Mahasiswa" href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $i; ?>/detil"><i class="fa fa-history"></i></a>
                                <a class="btn btn-sm btn-primary" title="Pengajuan KRS" href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2 . DS . $i; ?>/krs"><i class="fa fa-sitemap"></i></a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();