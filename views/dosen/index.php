<?php

namespace SIAKAD\Views\Dosen;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Dosen' )
    ->set_page_name( 'Dosen' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Ahmad Zafrullah
                <small>Data pribadi</small>
            </h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <select class="form-control">
                            <option>Cuti Menikah</option>
                            <option>Menerima Lamaran</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenjang</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <select class="form-control">
                            <option>S-1</option>
                            <option>S-2</option>
                            <option>S-3</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Shift</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <select class="form-control">
                            <option>Reguler</option>
                            <option>Non-Reguler</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tempat Lahir</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tanggal Lahir</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenis Kelamin</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <select class="form-control">
                            <option>Laki-laki</option>
                            <option>Perempuan</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tahun Masuk</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                        <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();