<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Pengumuman' )
    ->set_page_name( 'Pengumuman' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid pengumuman">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Pengumuman</h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <button class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</button>
                                    </div>
                                    <div class="col-sm-6">
                                        <select class="form-control">
                                            <option>--pilih kategori</option>
                                            <option>Berita</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <p>Semua isi dari pengumuman ini hanya bisa dilihat oleh tingkat <em>Operator Fakultas</em> saja.</p>
                    <div class="media">
                        <div class="pull-left tanggal">
                            <span class="hari">12</span> <span class="bulan">Juli</span><br/>
                            <span class="tahun">2014</span>
                        </div>
                        <div class="media-body">
                            <a href="#">
                                <h4 class="media-heading">Info KRS Online</h4>
                            </a>
                            <p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem...</p>
                            <span class="meta">
                                &mdash; <i class="fa fa-folder-o"></i> Berita&nbsp;&nbsp;
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/pengumuman/hapus/" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/pengumuman/perbaiki/" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                            </span>
                        </div>
                    </div>
                    <div class="media">
                        <div class="pull-left tanggal">
                            <span class="hari">9</span> <span class="bulan">Juli</span><br/>
                            <span class="tahun">2014</span>
                        </div>
                        <div class="media-body">
                            <a href="#">
                                <h4 class="media-heading">Launching SIAKAD</h4>
                            </a>
                            <p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem...</p>
                            <span class="meta">
                                &mdash; <i class="fa fa-folder-o"></i> Berita&nbsp;&nbsp;
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/pengumuman/hapus/" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/pengumuman/perbaiki/" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                            </span>
                        </div>
                    </div>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();