<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Pesan' )
    ->set_page_name( 'Pesan' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid pengumuman">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Pesan
                <small>Dosen Pembimbing Akademik</small>
            </h1>
            <div class="row">
                <div class="col-sm-12">
                    <div class="media">
                        <div class="pull-left tanggal">
                            <span class="hari">12</span> <span class="bulan">Juli</span><br/>
                            <span class="tahun">2014</span>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pak Dosen</h4>
                            <p>Cepet nikah biar lebih fokus belajar, jangan terlalu banyak kerja.</p>
                        </div>
                    </div>
                    <div class="media">
                        <div class="pull-left tanggal">
                            <span class="hari">12</span> <span class="bulan">Juli</span><br/>
                            <span class="tahun">2014</span>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Pak Dosen</h4>
                            <p>Cepet nikah biar lebih fokus belajar, jangan terlalu banyak kerja.</p>
                        </div>
                    </div>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();