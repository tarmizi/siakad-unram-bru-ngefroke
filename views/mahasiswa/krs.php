<?php

namespace SIAKAD\Views\Mahasiswa;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'KRS' )
    ->set_page_name( 'KRS' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );

$is_kelas = Routes::get_instance()->is_tingkat( 3, 'kelas' );
!$is_kelas || Headers::get_instance()->set_page_sub_name( 'Kelas' );
$is_peserta = $is_kelas && Routes::get_instance()->has_tingkat( 4 ) && Routes::get_instance()->is_tingkat( 5, 'peserta' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' ) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );


Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                <?php echo Headers::get_instance()->get_page_name(); ?>
                <small>Pengisian</small>
            </h1>
            <div class="alert alert-warning">
                <strong>Peringatan :</strong> side sudah semester 8, tapi sks masih 123.
            </div>
            <h3>F1B008004</h3>
            <table style="width: 100%">
                <thead>
                <tr>
                    <th>Tahun Ajaran</th>
                    <td>2014</td>
                </tr><tr>
                    <th>Semester</th>
                    <td>8 / Genap</td>
                </tr><tr>
                    <th>SKS yang telah diambil</th>
                    <td>123</td>
                </tr><tr>
                    <th>SKS yang boleh diambil</th>
                    <td>24</td>
                </tr>
                </thead>
            </table>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Kode</th>
                    <th>Nama</th>
                    <th>SKS</th>
                    <th>Prasyarat</th>
                    <th>#</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>MK12</td>
                    <td>Bahasa Sasak</td>
                    <td>4</td>
                    <td>
                        <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Prancis)</span><br/>
                        <span class="text-danger"><i class="glyphicon glyphicon-remove"></i> MK111 (Bahasa Jerman)</span><br/>
                    </td>
                    <td><input type="checkbox" disabled></td>
                </tr><tr>
                    <td>MK12</td>
                    <td>Bahasa India</td>
                    <td>4</td>
                    <td>
                        <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Inggris)</span><br/>
                        <span class="text-success"><i class="glyphicon glyphicon-ok"></i> MK111 (Bahasa Arab)</span><br/>
                    </td>
                    <td><input type="checkbox"></td>
                </tr>
                </tbody>
            </table>
            <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-send"></i> Ajukan</button>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();