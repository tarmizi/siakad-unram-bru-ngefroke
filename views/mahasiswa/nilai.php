<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Nilai' )
    ->set_page_name( 'Nilai' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Nilai
            </h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-sm-2">
                        <select class="form-control">
                            <option>--tahun ajaran</option>
                            <option>2010</option>
                            <option>2011</option>
                            <option>2012</option>
                        </select>
                    </div>
                    <div class="col-sm-2">
                        <select class="form-control">
                            <option>--mata kuliah</option>
                            <option>Bahasa Sasak</option>
                        </select>
                    </div>
                    <div class="col-sm-1">
                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-primary"><i class="fa fa-print"></i> Cetak</button>
                    </div>
                </div>
            </form>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th><input type="checkbox"></th>
                    <th>NIM</th>
                    <th>Nama</th>
                    <th>Nilai</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( range( 1, 9 ) as $i ) : ?>
                    <tr>
                        <td><input type="checkbox"></td>
                        <td>F1B00800<?php echo $i; ?></td>
                        <td>Ahmad Zafrullah</td>
                        <td>A</td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();