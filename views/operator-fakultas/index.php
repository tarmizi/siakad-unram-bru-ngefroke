<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Operator Fakultas' )
    ->set_page_name( 'Operator Fakultas' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Fakultas Teknik</h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Email</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="Alamat email">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <textarea class="form-control"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Telp</label>
                    <div class="col-xs-8 col-sm-5 col-lg-3">
                        <input type="text" class="form-control" placeholder="No telpon resmi">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                        <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();