<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Operator Fakultas' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="fa fa-institution"></i>&nbsp;&nbsp;Fakultas Teknik</a></li>
    <li <?php echo ( $page = 'Operator PRODI' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/operator/program-studi"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Daftar PRODI' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/prodi"><i class="fa fa-building"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Cetak' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/cetak"><i class="fa fa-print"></i> Cetak</a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'Transkrip' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/cetak/transkrip">Transkrip</a></li>
            <li <?php echo ( $page = 'Ijazah' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/cetak/ijazah">Ijazah</a></li>
        </ul>
    </li>
    <li <?php echo ( $page = 'Statistik' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/statistik"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Pengumuman' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pengumuman"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>