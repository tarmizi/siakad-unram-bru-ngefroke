<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Pengaturan' )
    ->set_page_name( 'Pengaturan' )
    ->set_page_sub_name( 'Akun' );

Contents::get_instance()->get_header();

$is_pengaturan_akun = Routes::get_instance()->is_tingkat( 3, 'akun' );


?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_pengaturan_akun ) : ?>
                <h1 class="page-header">
                    <?php echo Headers::get_instance()->get_page_name(); ?>
                    <small><?php echo Headers::get_instance()->get_page_sub_name(); ?></small>
                </h1>
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama Lengkap</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input type="text" class="form-control">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <textarea class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">No. HP</label>
                        <div class="col-xs-8 col-sm-5 col-lg-3">
                            <input type="text" class="form-control" placeholder="No ponsel pribadi">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                        </div>
                    </div>
                </form>
            <?php else : ?>
                <h1 class="page-header">Pengaturan</h1>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();