<?php

namespace SIAKAD\Views\Operator\Fakultas;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Program Studi' )
    ->set_page_name( 'Daftar PRODI' );

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <div class="row">
                <div class="col-xs-12">
                    <?php if( $is_tambah ) : ?>
                        <h1 class="page-header">Tambah Program Studi</h1>
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Kode Nasional</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <input type="text" class="form-control" placeholder="Kode nasional">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Kode PRODI UNRAM</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <input type="text" class="form-control" placeholder="Kode dari UNRAM">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama Nasional</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <input type="text" class="form-control" placeholder="Nama dari nasional">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama PRODI UNRAM</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <input type="text" class="form-control" placeholder="Nama dari UNRAM">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama Inggris</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <input type="text" class="form-control" placeholder="Nama dalam bahasa inggris">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenjang</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <select class="form-control">
                                        <option>--pilih--</option>
                                        <option>A</option>
                                        <option>B</option>
                                        <option>C</option>
                                        <option>D</option>
                                        <option>E</option>
                                        <option>F</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Keterangan</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
                                <div class="col-xs-8 col-sm-5 col-lg-3">
                                    <textarea class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                                    <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                </div>
                            </div>
                        </form>
                    <?php else : ?>
                        <h1 class="page-header">Daftar Program Studi</h1>
                        <div class="row">
                            <div class="col-sm-2">
                                <a href="<?php echo SIAKAD_URI_PATH . DS . $tingkat1 . DS . $tingkat2; ?>/tambah" class="btn btn-primary"><i class="fa fa-pencil-square-o"></i> Baru</a>
                            </div>
                            <div class="col-md-7">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-3 control-label">Yang dicentang</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--pilih aksi</option>
                                                <option>Hapus masal</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-3">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Kode Prodi</th>
                                <th>Nama PRODI</th>
                                <th>Alamat</th>
                                <th>Jenjang</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>FB</td>
                                <td>Teknik Elektrom</td>
                                <td>Jl. Majapahit No. 62 Matara</td>
                                <td>C</td>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/hapus" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/perbaiki" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();