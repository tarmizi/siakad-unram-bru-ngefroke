<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Autentikasi' );

$is_lupa_kata_sandi = Routes::get_instance()->is_tingkat( 2, 'lupa-kata-sandi' );
$is_daftar_baru = Routes::get_instance()->is_tingkat( 2, 'daftar-baru' );
$is_masuk = Routes::get_instance()->is_tingkat( 2, 'masuk' );
$is_keluar = Routes::get_instance()->is_tingkat( 2, 'keluar' );

Contents::get_instance()->get_header();

?>

    <div class="container autentikasi">
        <div class="row">
            <div class="col-lg-6 col-lg-offset-3">
                <?php if( $is_masuk ) : ?>
                    <div class="alert alert-success">
                        <p>Anda berhasil login, but wait...</p>
                    </div>
                    <p>Sementara masih dalam proses, untuk jalan pintas bisa menggunakan tautan berikut:</p>
                    <ul>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/admin">Admin</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/akademik">Akademik</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/dosen">Dosen</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/operator-siakad">Operator (SIAKAD)</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/operator-fakultas">Operator (Fakukltas)</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/operator-prodi">Operator (Program Studi)</a></li>
                        <li><a href="<?php echo SIAKAD_URI_PATH; ?>/mahasiswa">Mahasiswa</a></li>
                    </ul>
                <?php elseif( $is_lupa_kata_sandi ) : ?>
                    <h1>Autentikasi</h1>
                    <p>Permintaan kata sandi baru, silakan lengkapi isian berikut:</p>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">ID Pengguna</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="ID Pengguna">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Nama Lengkap</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Nama Lengkap">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-4 control-label">Alamat Email</label>
                            <div class="col-sm-6">
                                <input type="text" class="form-control" placeholder="Email aktif">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-6">
                                <button type="submit" class="btn btn-primary btn-block">Kirim</button>
                            </div>
                        </div>
                    </form>
                <?php elseif( $is_keluar ) : ?>
                    <div class="alert alert-warning">
                        <p><i class="fa fa-warning"></i> Anda berhasil keluar</p>
                    </div>
                    <a href="<?php echo SIAKAD_URI_PATH; ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-home"></i> Beranda</a>
                <?php else : ?>
                    <div class="alert alert-warning">
                        <p><i class="glyphicon glyphicon-remove"></i> <strong>Ehmmm... </strong>halaman ini hanya bagi developer yang masih lajang /o/</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();