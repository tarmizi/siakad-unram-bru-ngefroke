<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Footers;

Footers::get_instance()
    ->add_script( 'bootstrap.min.js', Headers::add_position_before )
    ->add_script( 'jquery.ui.min.js', Headers::add_position_before )
    ->add_script( 'jquery.min.js', Headers::add_position_before );

?>
    <hr>
    <div class="container">
        <footer>
            <p>&copy; Hak Cipta <?php echo date( 'Y' ); ?> <?php echo SIAKAD_APP_AUTHOR; ?></p>
        </footer>
    </div>

    <?php echo Footers::get_instance()->get_script(); ?>
</body>
</html>