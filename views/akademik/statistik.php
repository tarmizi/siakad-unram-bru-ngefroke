<?php

namespace SIAKAD\Views\Operator\Akademik;

use SIAKAD\Controller\Footers;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Statistik' )
    ->set_page_name( 'Statistik' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Statistik</h1>
            <div class="row">
                <div class="col-md-6">
                    <div id="chart-dosen" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <div class="col-md-6">
                    <div id="chart-mahasiswa" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div id="chart-kelulusan" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
                <div class="col-md-6">
                    <div id="chart-penerima-beasiswa" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Footers::get_instance()
    ->add_script( 'highcharts.js' )
    ->add_script( 'operator.prodi.statistik.js' );
Contents::get_instance()->get_footer();