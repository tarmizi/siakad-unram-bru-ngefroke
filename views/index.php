<?php

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Beranda' );

Contents::get_instance()->get_header();

?>

<div class="jumbotron unram-splash">
    <div class="container">
        <h1>Go online!</h1>
        <p>Sistem Informasi Akademik (SIAKAD) menyediakan cara mudah bagi mahasiswa untuk melakukan pengisian KRS secara cepat kapanpun, dimanapun.</p>
        <p><a href="<?php echo SIAKAD_URI_PATH; ?>/info/berita/kenapa-siakad-lebih-baik" class="btn btn-success btn-lg">Kenapa?</a></p>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-4">
            <h2>Info Akademik</h2>
            <p>Dalam menyambut bulan Ramadhan tahun ini pihak akademik menghimbau kepada seluruh mahasiswa (muslim) untuk mengenakan pakaian muslim, khususnya yang laki-laki agar menggunakan sarung.</p>
            <p><a class="btn btn-default" href="<?php echo SIAKAD_URI_PATH; ?>/info/berita/kenapa-siakad-lebih-baik" role="button">Selengkapnya &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Pengumuman</h2>
            <p>Pihak rektorat sedang menyusun kurikulum baru dimana semua mahasiswa yang ada di fakultas hukun diwajibkan untuk menguasai bahasa pemrograman Assembly.</p>
            <p><a class="btn btn-default" href="<?php echo SIAKAD_URI_PATH; ?>/info/berita/kenapa-siakad-lebih-baik" role="button">Selengkapnya &raquo;</a></p>
        </div>
        <div class="col-md-4">
            <h2>Berita Terkini</h2>
            <ul>
                <li><a href="<?php echo SIAKAD_URI_PATH; ?>/info/berita/kenapa-siakad-lebih-baik">Seorang mahasiswa teknik informatika pingsan setelah kuliah bahasa pemrograman</a></li>
                <li><a href="<?php echo SIAKAD_URI_PATH; ?>/info/berita/kenapa-siakad-lebih-baik">Mahasiswa teknik informatika berunjuk rasa menuntut perubahan kurikulum</a></li>
            </ul>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();