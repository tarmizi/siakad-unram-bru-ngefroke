<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Musim Akademik' )
    ->set_page_name( 'Musim Akademik' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid musim-akademik">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Musim Akademik</h1>
            <form class="form-horizontal">
                <label class="col-md-2 control-label">Event yang aktif</label>
                <div class="col-xs-8 col-md-4">
                    <select class="form-control">
                        <option>--pilih event</option>
                        <option>KRS</option>
                        <option>Perubahan KRS</option>
                        <option>Pengisian Nilai Akademik</option>
                        <option>Pengisian Data Kewisudawan</option>
                    </select>
                </div>
                <div class="col-xs-4 col-md-2">
                    <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                </div>
            </form>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();