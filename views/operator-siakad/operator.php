<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Operator SIAKAD' )
    ->set_page_name( 'Operator' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$is_operator_fakultas = Routes::get_instance()->is_tingkat( 3, 'fakultas' );

$is_tambah = Routes::get_instance()->is_tingkat( 4, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 4, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 4, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 4, 'hapus' );

if( $is_operator_fakultas ) Headers::get_instance()->set_page_sub_name( 'Operator Fakultas' );
else Headers::get_instance()->set_page_sub_name( 'SIAKAD' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">

            <h1 class="page-header">Operator Fakultas</h1>

            <?php if( $is_hapus ) : ?>
                <div class="alert alert-warning">
                    <p><i class="glyphicon glyphicon-ok"></i> Operator berhasil dihapus.</p>
                </div>
            <?php elseif( $is_simpan ) : ?>
                <div class="alert alert-success">
                    <p><i class="glyphicon glyphicon-ok"></i> Operator berhasil disimpan.</p>
                </div>
            <?php elseif( $is_perbaiki ) : ?>
                <div class="alert alert-info">
                    <p><i class="glyphicon glyphicon-ok"></i> Operator berhasil perbaharui.</p>
                </div>
            <?php endif; ?>


            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Yang dicentang</label>
                                    <div class="col-sm-4">
                                        <select class="form-control">
                                            <option>--pilih aksi</option>
                                            <option>Hapus masal</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox"></th>
                            <th>NIP</th>
                            <th>Nama</th>
                            <th>Fakultas</th>
                            <th>No. HP</th>
                            <th>Email</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach( range( 1, 10 ) as $i ) : ?>
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>1234-<?php echo $i; ?></td>
                                <td>Ahmad Zafrullah</td>
                                <td>Fakultas Teknik</td>
                                <td>08123456<?php echo $i; ?></td>
                                <td>zaf@elektro08.com</td>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong><i class="glyphicon glyphicon-plus"></i> Tambah Operator</strong>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/simpan" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">NIP</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="NIP">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Fakultas</label>
                                    <div class="col-sm-8">
                                        <select class="form-control">
                                            <option>--</option>
                                            <option>Fakutlas Teknik</option>
                                            <option>Fakutlas Hukum</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">No. HP</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Nomor Handphone">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Email</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Alamat email">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();