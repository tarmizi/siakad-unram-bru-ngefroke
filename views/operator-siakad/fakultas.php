<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Routes;
use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Fakultas' )
    ->set_page_name( 'Daftar Fakultas' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Daftar Fakultas</h1>
            <?php if( $is_hapus ) : ?>
                <div class="alert alert-warning">
                    <p><i class="glyphicon glyphicon-ok"></i> Fakultas berhasil dihapus.</p>
                </div>
            <?php elseif( $is_simpan ) : ?>
                <div class="alert alert-success">
                    <p><i class="glyphicon glyphicon-ok"></i> Fakultas berhasil disimpan.</p>
                </div>
            <?php elseif( $is_perbaiki ) : ?>
                <div class="alert alert-info">
                    <p><i class="glyphicon glyphicon-ok"></i> Fakultas berhasil perbaharui.</p>
                </div>
            <?php endif; ?>
            <div class="row">
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-md-8">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Yang dicentang</label>
                                    <div class="col-sm-4">
                                        <select class="form-control">
                                            <option>--pilih aksi</option>
                                            <option>Hapus masal</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox"></th>
                            <th>Kode</th>
                            <th>Nama Fakultas</th>
                            <th>Alias</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach( range( 1, 10 ) as $i ) : ?>
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>F-<?php echo $i; ?></td>
                                <td>Fakultas Teknik ke <?php echo $i; ?></td>
                                <td>FT-<?php echo $i; ?></td>
                                <td>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <strong><i class="glyphicon glyphicon-plus"></i> Tambah Fakultas</strong>
                        </div>
                        <div class="panel-body">
                            <form class="form-horizontal" role="form" action="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>/fakultas/simpan" method="post">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Kode</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Kode fakultas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Nama</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Nama fakultas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Alias</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" placeholder="Alias fakultas">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-4 control-label">Keterangan</label>
                                    <div class="col-sm-8">
                                        <textarea class="form-control" placeholder="Keterangan fakultas"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-8 col-sm-offset-4">
                                        <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();