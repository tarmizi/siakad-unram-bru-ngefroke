<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Operator SIAKAD' )
    ->set_page_name( 'Operator SIAKAD' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">Operator SIAKAD</h1>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();