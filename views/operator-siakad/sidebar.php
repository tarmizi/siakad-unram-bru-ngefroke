<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Operator SIAKAD' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="glyphicon glyphicon-user"></i>&nbsp;&nbsp;Ahmad Zafrullah</a></li>
    <li <?php echo ( $page = 'Operator Fakultas' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/operator/fakultas"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Daftar Fakultas' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/fakultas"><i class="fa fa-institution"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Pengumuman' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pengumuman"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Musim Akademik' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/akademik"><i class="fa fa-calendar"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>