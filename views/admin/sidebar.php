<?php

namespace SIAKAD\Views\Operator\SIAKAD;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Admin' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="fa fa-linux"></i>&nbsp;&nbsp;Admin</a></li>
    <li <?php echo ( $page = 'Operator SIAKAD' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/operator/siakad"><i class="fa fa-users"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Log Nilai' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/log-nilai"><i class="fa fa-history"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>