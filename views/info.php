<?php
/**
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Views\Publik;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;

Headers::get_instance()
    ->set_page_title( 'Pusat Informasi' );

Contents::get_instance()->get_header();

?>

    <div class="container info">
        <div class="row">
            <div class="col-sm-8 main">
                <div class="item">
                    <h2 class="title">Saya tidak bisa login?</h2>
                    <div class="content">
                        <p>Lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem ipsum lorem</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 sidebar">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <strong>Bantuan lainnya</strong>
                    </div>
                    <div class="panel-body">
                        <ul>
                            <li><a href="#">Pacar sering marah-marah?</a></li>
                            <li><a href="#">Pacar kadang selingkuh?</a></li>
                            <li><a href="#">Pacar selalu ngilang?</a></li>
                            <li><a href="#">Baru sadar tidak punya pacar?</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php Contents::get_instance()->get_footer();