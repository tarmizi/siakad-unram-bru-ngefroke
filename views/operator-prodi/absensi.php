<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Absensi' )
    ->set_page_name( 'Absensi' )
    ->set_page_sub_name( 'absmhs');

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$is_absen_dosen = Routes::get_instance()->is_tingkat( 3, 'dosen' );
$is_absen_mahasiswa = Routes::get_instance()->is_tingkat( 3, 'mahasiswa' );

!$is_absen_dosen || Headers::get_instance()->set_page_sub_name( 'absds' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Absensi
                <small><?php if( $is_absen_dosen ) : ?>Dosen<?php else : ?>Mahasiswa<?php endif; ?></small>
            </h1>
            <form class="form-horizontal">
                <div class="form-group">
                    <label class="col-sm-2 control-label">Pilih tanggal</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control">
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control">
                            <option>--bidang keahlian</option>
                            <option>Sistem Cerdas</option>
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                    </div>
                </div>
            </form>
            <br/>
            <table class="table table-bordered table-striped">
                <thead>
                <tr>
                    <th>Kode Mata Kuliah</th>
                    <th>Nama Mata Kuliah</th>
                    <th>Semester</th>
                    <th>Kelas</th>
                    <th>Hadir?</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach( range( 1, 3 ) as $i ) : ?>
                    <tr>
                        <td>MK123</td>
                        <td>Bahasa Sasak</td>
                        <td>8</td>
                        <td>A</td>
                        <td><input type="checkbox"></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();