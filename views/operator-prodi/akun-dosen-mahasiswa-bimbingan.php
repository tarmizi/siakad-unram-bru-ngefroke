<?php

use SIAKAD\Controller\Contents;

?>

<h1 class="page-header">
    Dosen
    <small>Mahasiswa Bimbingan</small>
</h1>
<div class="row">
    <div class="col-sm-8">
        <div class="row">
            <div class="col-md-8">
                <form class="form-horizontal">
                    <div class="form-group">
                        <div class="col-sm-4">
                            <select class="form-control">
                                <option>--kurikulum</option>
                                <option>2011</option>
                                <option>2012</option>
                                <option>2013</option>
                            </select>
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4">
                <form>
                    <div class="input-group">
                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                    </div>
                </form>
            </div>
        </div>
        <br/>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th><input type="checkbox"></th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Semester</th>
                <th>#</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach( range( 1, 3 ) as $i ) : ?>
                <tr>
                    <td><input type="checkbox"></td>
                    <td>F1B008004</td>
                    <td>Ahmad Zafrullah</td>
                    <td>Laki-laki</td>
                    <td>8</td>
                    <td><a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a></td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
        <ul class="pagination">
            <li><a href="#">&laquo;</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#">5</a></li>
            <li><a href="#">&raquo;</a></li>
        </ul>
    </div>
    <div class="col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <strong><i class="glyphicon glyphicon-plus"></i> Mahasiswa Bimbingan</strong>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" role="form" action="" method="post">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">NIM</label>
                        <div class="col-sm-8">
                            <select class="form-control">
                                <option>F1B008004 (Ahmad Zafrullah)</option>
                                <option>F1B008004 (Ahmad Zafrullah)</option>
                                <option>F1B008004 (Ahmad Zafrullah)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-8 col-sm-offset-4">
                            <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Tambah</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>