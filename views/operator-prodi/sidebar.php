<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Contents;

$base_link = SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view(); ?>
<ul class="nav nav-sidebar">
    <li <?php echo 'Operator PRODI' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>"><i class="fa fa-building"></i>&nbsp;&nbsp;Teknik Informatika</a></li>
    <li <?php echo 'Daftar Akun' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/akun"><i class="fa fa-users"></i>&nbsp;&nbsp;Daftar Akun</a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'Akademik' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/akun/akademik"><?php echo $page; ?></a></li>
            <li <?php echo ( $page = 'Dosen' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/akun/dosen"><?php echo $page; ?></a></li>
            <li <?php echo ( $page = 'Mahasiswa' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/akun/mahasiswa"><?php echo $page; ?></a></li>
        </ul>
    </li>
    <li <?php echo ( $page = 'Mata Kuliah' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/mata-kuliah"><i class="fa fa-book"></i>&nbsp;&nbsp;<?php echo $page; ?></a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'Mata Kuliah' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/mata-kuliah/daftar"><?php echo $page; ?></a></li>
            <li <?php echo ( $page = 'Kelas' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/mata-kuliah/kelas"><?php echo $page; ?></a></li>
        </ul>
    </li>
    <li <?php echo 'Kurikulum' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/kurikulum"><i class="fa fa-sitemap"></i>&nbsp;&nbsp;Kurikulum</a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'Kurikulum' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/kurikulum/daftar"><?php echo $page; ?></a></li>
            <li <?php echo ( $page = 'Konversi' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/kurikulum/konversi"><?php echo $page; ?></a></li>
        </ul>
    </li>
    <li <?php echo 'Absensi' == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/absensi"><i class="fa fa-calendar-o"></i>&nbsp;&nbsp;Absensi</a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'absds' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/absensi/dosen">Dosen</a></li>
            <li <?php echo ( $page = 'absmhs' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/absensi/mahasiswa">Mahasiswa</a></li>
        </ul>
    </li>
    <li <?php echo ( $page = 'Nilai' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/nilai"><i class="fa fa-database"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Data Wisudawan' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/nilai"><i class="fa fa-graduation-cap"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Cetak' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> >
        <a href="<?php echo $base_link; ?>/cetak"><i class="fa fa-print"></i>&nbsp;&nbsp;Cetak</a>
        <ul class="nav nav-sidebar">
            <li <?php echo ( $page = 'Transkrip' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/cetak/transkrip">Transkrip</a></li>
            <li <?php echo ( $page = 'Ijazah' ) == Headers::get_instance()->get_page_sub_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/cetak/ijazah">Ijazah</a></li>
        </ul>
    </li>
    <li <?php echo ( $page = 'Statistik' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/statistik"><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
    <li <?php echo ( $page = 'Pengumuman' ) == Headers::get_instance()->get_page_name() ? 'class="active"' : ''; ?> ><a href="<?php echo $base_link; ?>/pengumuman"><i class="fa fa-clipboard"></i>&nbsp;&nbsp;<?php echo $page; ?></a></li>
</ul>