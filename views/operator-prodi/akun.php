<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Daftar Akun' )
    ->set_page_name( 'Daftar Akun' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$is_akun_akademik = Routes::get_instance()->is_tingkat( 3, 'akademik' );
$is_akun_dosen = Routes::get_instance()->is_tingkat( 3, 'dosen' );
$is_akun_mahasiswa = Routes::get_instance()->is_tingkat( 3, 'mahasiswa' );

$is_tambah = Routes::get_instance()->is_tingkat( 4, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 4, 'perbaiki' ) && Routes::get_instance()->has_tingkat( 5 );
$is_simpan = Routes::get_instance()->is_tingkat( 4, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 4, 'hapus' );
$is_kirim_pesan = Routes::get_instance()->is_tingkat( 4, 'kirim-pesan' ) && Routes::get_instance()->has_tingkat( 5 );
$is_mahasiswa_bimbingan = Routes::get_instance()->is_tingkat( 4, 'mahasiswa-bimbingan' ) && Routes::get_instance()->has_tingkat( 5 );

if( $is_akun_dosen ) Headers::get_instance()->set_page_sub_name( 'Dosen' );
elseif( $is_akun_mahasiswa ) Headers::get_instance()->set_page_sub_name( 'Mahasiswa' );
else Headers::get_instance()->set_page_sub_name( 'Akademik' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_tambah || $is_perbaiki ) :
                if( $is_akun_mahasiswa ) Contents::get_instance()->get_template( 'akun-mahasiswa' );
                elseif( $is_akun_dosen ) Contents::get_instance()->get_template( 'akun-dosen' );
                else Contents::get_instance()->get_template( 'akun-akademik' );
            elseif( $is_kirim_pesan && $is_akun_mahasiswa ) :
                Contents::get_instance()->get_template( 'akun-mahasiswa-kirim-pesan' );
            elseif( $is_mahasiswa_bimbingan && $is_akun_dosen ) :
                Contents::get_instance()->get_template( 'akun-dosen-mahasiswa-bimbingan' );
            else : ?>
                <h1 class="page-header">
                    Daftar Akun
                    <small><?php echo Headers::get_instance()->get_page_sub_name(); ?></small>
                </h1>
                <div class="row">
                    <div class="col-md-9">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <div class="col-sm-2">
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>--angkatan</option>
                                        <option>2013</option>
                                        <option>2014</option>
                                        <option>2015</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control">
                                        <option>--bidang keahlian</option>
                                        <option>Sistem Cerdas</option>
                                    </select>
                                </div>
                                <div class="col-sm-1">
                                    <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-3">
                        <form>
                            <div class="input-group">
                                <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                            </div>
                        </form>
                    </div>
                </div>
                <br/>
                <table class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th><input type="checkbox"></th>
                        <th>NIP</th>
                        <th>Nama</th>
                        <th>Jabatan</th>
                        <th>No. HP</th>
                        <th>Email</th>
                        <th>#</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach( range( 1, 10 ) as $i ) : ?>
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>1234-<?php echo $i; ?></td>
                            <td>Ahmad Zafrullah</td>
                            <td>Dekan</td>
                            <td>08123456<?php echo $i; ?></td>
                            <td>zaf@elektro08.com</td>
                            <td>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                <?php if( $is_akun_mahasiswa ) : ?>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/kirim-pesan/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-envelope"></i></a>
                                <?php endif; ?>
                                <?php if( $is_akun_dosen ) : ?>
                                    <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/mahasiswa-bimbingan/<?php echo $i; ?>" title="Mahasiswa Bimbingan"><i class="fa fa-wheelchair"></i></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <ul class="pagination">
                    <li><a href="#">&laquo;</a></li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">&raquo;</a></li>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();