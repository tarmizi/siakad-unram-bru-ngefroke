<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Kurikulum' )
    ->set_page_name( 'Kurikulum' )
    ->set_page_sub_name( 'Kurikulum');

$tingkat1 = Routes::get_instance()->get_tingkat( 1 );
$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );

$is_konversi_kurikulum = Routes::get_instance()->is_tingkat( 3, 'konversi' );

!$is_konversi_kurikulum || Headers::get_instance()->set_page_sub_name( 'Konversi' );

Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <h1 class="page-header">
                Kurikulum
                <small><?php if( $is_konversi_kurikulum ) : ?>Konversi<?php else : ?>Daftar<?php endif; ?></small>
            </h1>
            <?php if( $is_konversi_kurikulum ) : ?>
                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--kurikulum</option>
                                                <option>2011</option>
                                                <option>2012</option>
                                                <option>2013</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Kode Baru</th>
                                <th>Nama Baru</th>
                                <th>Asal</th>
                                <th>Semester</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach( range( 1, 3 ) as $i ) : ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>MK123</td>
                                    <td>Bahasa Sasak</td>
                                    <td>
                                        <ul>
                                            <li>MK111 (Bahasa Prancis)</li>
                                            <li>MK222 (Bahasa Jerman)</li>
                                            <li>MK333 (Bahasa Jepang)</li>
                                        </ul>
                                    </td>
                                    <td>Genap</td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong><i class="glyphicon glyphicon-plus"></i> Konversi Mata Kuliah</strong>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Kode Baru</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Kode MK baru">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Nama Baru</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" placeholder="Nama MK baru">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Asal</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>MK111 (Bahasa Prancis)</option>
                                                <option>MK222 (Bahasa Jerman)</option>
                                                <option>MK333 (Bahasa Jepang)</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Semester</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Genam</option>
                                                <option selected>Ganjil</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Status</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Aktif</option>
                                                <option selected>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else : ?>

                <div class="row">
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-md-8">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Yang dicentang</label>
                                        <div class="col-sm-4">
                                            <select class="form-control">
                                                <option>--pilih aksi</option>
                                                <option>Hapus masal</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button class="btn btn-primary"><i class="fa fa-legal"></i> OK</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-4">
                                <form>
                                    <div class="input-group">
                                        <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <br/>
                        <table class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th><input type="checkbox"></th>
                                <th>Status</th>
                                <th>Tahun</th>
                                <th>Semester</th>
                                <th>#</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach( range( 1, 3 ) as $i ) : ?>
                                <tr>
                                    <td><input type="checkbox"></td>
                                    <td>Aktif</td>
                                    <td><?php echo $i+2010; ?></td>
                                    <td>Genap</td>
                                    <td>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                        <ul class="pagination">
                            <li><a href="#">&laquo;</a></li>
                            <li><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><a href="#">5</a></li>
                            <li><a href="#">&raquo;</a></li>
                        </ul>
                    </div>
                    <div class="col-sm-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <strong><i class="glyphicon glyphicon-plus"></i> Kurikulum Baru</strong>
                            </div>
                            <div class="panel-body">
                                <form class="form-horizontal" role="form" action="" method="post">
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Tahun</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>-- tahun</option>
                                                <option>2008</option>
                                                <option>2013</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Semester</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Genam</option>
                                                <option selected>Ganjil</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-4 control-label">Status</label>
                                        <div class="col-sm-8">
                                            <select class="form-control">
                                                <option>Aktif</option>
                                                <option selected>Tidak Aktif</option>
                                            </select>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();