<?php

namespace SIAKAD\Views\Operator\PRODI;

use SIAKAD\Controller\Contents;
use SIAKAD\Controller\Headers;
use SIAKAD\Controller\Routes;

Headers::get_instance()
    ->set_page_title( 'Mata Kuliah' )
    ->set_page_name( 'Mata Kuliah' )
    ->set_page_sub_name( 'Mata Kuliah' );

$tingkat2 = Routes::get_instance()->get_tingkat( 2 );
$tingkat3 = Routes::get_instance()->get_tingkat( 3 );
$tingkat4 = Routes::get_instance()->get_tingkat( 4 );

$is_kelas = Routes::get_instance()->is_tingkat( 3, 'kelas' );
!$is_kelas || Headers::get_instance()->set_page_sub_name( 'Kelas' );
$is_peserta = $is_kelas && Routes::get_instance()->has_tingkat( 4 ) && Routes::get_instance()->is_tingkat( 5, 'peserta' );

$is_tambah = Routes::get_instance()->is_tingkat( 3, 'tambah' );
$is_perbaiki = Routes::get_instance()->is_tingkat( 3, 'perbaiki' ) && Routes::get_instance()->has_tingkat( 4 );
$is_simpan = Routes::get_instance()->is_tingkat( 3, 'simpan' );
$is_hapus = Routes::get_instance()->is_tingkat( 3, 'hapus' );


Contents::get_instance()->get_header();

?>

<div class="container-fluid">
    <div class="row">
        <div class="col-xs-3 col-sm-2 sidebar">
            <?php Contents::get_instance()->get_sidebar(); ?>
        </div>
        <div class="col-xs-9 col-sm-10 main">
            <?php if( $is_kelas ) :
                if( $is_peserta ) : ?>
                    <h1 class="page-header">
                        Kelas A
                        <small>Peserta</small>
                    </h1>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox"></th>
                            <th>NIM</th>
                            <th>Nama</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach( range( 1, 5 ) as $i ) : ?>
                            <tr>
                                <td><input type="checkbox"></td>
                                <td>1234-<?php echo $i; ?></td>
                                <td>Ahmad Zafrullah</td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option>--pindah</option>
                                            <option>Kelas B</option>
                                            <option>Kelas C</option>
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                <?php else : ?>
                    <h1 class="page-header">Mata Kuliah <small>Kelas</small></h1>
                    <div class="row">
                        <div class="col-md-9">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>--semester</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>--bidang keahlian</option>
                                            <option>Sistem Cerdas</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-sm-8">
                            <table class="table table-bordered table-striped">
                                <thead>
                                <tr>
                                    <th><input type="checkbox"></th>
                                    <th>Mata Kuliah</th>
                                    <th>Kelas</th>
                                    <th>Dosen</th>
                                    <th>Peserta saat ini</th>
                                    <th>#</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach( range( 1, 3 ) as $i ) : ?>
                                    <tr>
                                        <td><input type="checkbox"></td>
                                        <td>Bahasa Sasak</td>
                                        <td>A-<?php echo $i; ?></td>
                                        <td>Ahmad Zafrullah</td>
                                        <td>32</td>
                                        <td>
                                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/perbaiki/<?php echo $i; ?>" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3; ?>/hapus/<?php echo $i; ?>" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                                            <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2 . DS . $tingkat3 . DS . $i; ?>/peserta" title="Daftar Peserta"><i class="fa fa-users"></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-sm-4">
                            <div class="panel panel-primary">
                                <div class="panel-heading">
                                    <strong><i class="glyphicon glyphicon-plus"></i> Tambah Kelas</strong>
                                </div>
                                <div class="panel-body">
                                    <form class="form-horizontal" role="form" action="" method="post">
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Nama Kelas</label>
                                            <div class="col-sm-8">
                                                <input type="text" class="form-control">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Dosen</label>
                                            <div class="col-sm-8">
                                                <select class="form-control">
                                                    <option>Ahmad Zafrullah</option>
                                                    <option>Afiuw Zumala</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-4 control-label">Mata Kuliah</label>
                                            <div class="col-sm-8">
                                                <select class="form-control">
                                                    <option>Bahasa Sasak</option>
                                                    <option>Bahasa Jerman</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-sm-8 col-sm-offset-4">
                                                <button class="btn btn-sm btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php else : ?>
                <?php if( $is_tambah || $is_perbaiki ) : ?>
                    <h1 class="page-header">Mata Kuliah <small>Tambah</small></h1>
                    <form class="form-horizontal">
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Kode MK</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jumlah SKS</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <input type="text" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Sifat</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <select class="form-control">
                                    <option>Wajib</option>
                                    <option>Makruh</option>
                                    <option>Sunnah</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Semester</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <select class="form-control">
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Konsentrasi</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <select class="form-control">
                                    <option>Sistem Cerdas</option>
                                    <option>Sistem Manual</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Prasyarat</label>
                            <div class="col-xs-8 col-sm-5 col-lg-3">
                                <label><input type="checkbox"> Bahasa Prancis</label><br/>
                                <label><input type="checkbox"> Bahasa Jerman</label><br/>
                                <label><input type="checkbox"> Bahasa Program</label><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
                                <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
                            </div>
                        </div>
                    </form>
                <?php else : ?>
                    <h1 class="page-header">
                        <?php echo Headers::get_instance()->get_page_name(); ?>
                        <small>Daftar</small>
                    </h1>
                    <div class="row">
                        <div class="col-md-9">
                            <form class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-sm-2">
                                        <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah</a>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>--semester</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-3">
                                        <select class="form-control">
                                            <option>--bidang keahlian</option>
                                            <option>Sistem Cerdas</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-1">
                                        <button class="btn btn-primary"><i class="fa fa-filter"></i> OK</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3">
                            <form>
                                <div class="input-group">
                                    <input type="text" class="form-control">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"><i class="glyphicon glyphicon-search"></i> Cari</button>
                                    </span>
                                </div>
                            </form>
                        </div>
                    </div>
                    <br/>
                    <table class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th><input type="checkbox"></th>
                            <th>Kode</th>
                            <th>Nama</th>
                            <th>SKS</th>
                            <th>Sifat</th>
                            <th>#</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><input type="checkbox"></td>
                            <td>MK12</td>
                            <td>Bahasa Sasak</td>
                            <td>4</td>
                            <td>Wajib</td>
                            <td>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/perbaiki/as" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/hapus/as" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr><tr>
                            <td><input type="checkbox"></td>
                            <td>MK12</td>
                            <td>Bahasa Sasak</td>
                            <td>4</td>
                            <td>Wajib</td>
                            <td>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/perbaiki/as" title="Perbaiki"><i class="glyphicon glyphicon-pencil"></i></a>
                                <a href="<?php echo SIAKAD_URI_PATH . DS . Contents::get_instance()->get_view() . DS . $tingkat2; ?>/hapus/as" title="Hapus"><i class="glyphicon glyphicon-remove"></i></a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                    <ul class="pagination">
                        <li><a href="#">&laquo;</a></li>
                        <li><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li><a href="#">5</a></li>
                        <li><a href="#">&raquo;</a></li>
                    </ul>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
</div>

<?php Contents::get_instance()->get_footer();