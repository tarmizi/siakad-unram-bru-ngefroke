<h1 class="page-header">Dosen</h1>
<form class="form-horizontal">
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Nama</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Status</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <select class="form-control">
                <option>Cuti Menikah</option>
                <option>Menerima Lamaran</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenjang</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <select class="form-control">
                <option>S-1</option>
                <option>S-2</option>
                <option>S-3</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Shift</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <select class="form-control">
                <option>Reguler</option>
                <option>Non-Reguler</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tempat Lahir</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tanggal Lahir</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <input type="text" class="form-control">
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Alamat</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <textarea class="form-control"></textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Jenis Kelamin</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <select class="form-control">
                <option>Laki-laki</option>
                <option>Perempuan</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label class="col-xs-4 col-sm-3 col-lg-2 control-label">Tahun Masuk</label>
        <div class="col-xs-8 col-sm-5 col-lg-3">
            <input type="text" class="form-control">
        </div>
    </div>

    <!-- Button trigger modal -->
    <a class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
        Launch demo modal
    </a>

    <div class="form-group">
        <div class="col-xs-8 col-xs-offset-4 col-sm-5 col-sm-offset-3 col-lg-3 col-lg-offset-2">
            <button class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i> Simpan</button>
        </div>
    </div>
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>