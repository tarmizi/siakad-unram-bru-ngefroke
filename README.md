Sistem Informasi Akademik Universitas Mataram
=============================================

Ini adalah prototype (kerangka) struktur program agar `ngoding berjamaah` dapat berjalan dengan khusu'
dan tidak menyimpang dari jalan kebenaran /o/

##Persiapan
* lakukan konfigurasi terhadap file `define.path.php` dan `define.db.php` didalam direktory `includes`
* pastikan `mod_rewrite` apache aktif untuk penggunaan `.htaccess`
* database yang digunakan adalah dari file `siakad_unram (8 juni 2014).mwb` revisi terakhir dari pak Ario
* export `siakad_unram (8 juni 2014).mwb` kedalam bentuk `file.sql` menggunakan MySQL Workbench lalu import ke dbms (mysql) local

###Khusus Progs
* `Fork` repo
* `Clone` ke Local Branch
* Buat `Pull Request` untuk men-submit perubahan

##Commits (Devs)
* ignore (.gitignore) file `define.db.php` dan `define.path.php` agar tidak mengganggu konfigurasi default
* ignore juga direktory dari tool yang sedang digunakan (composer, phpunit, dll)

###Lainnya
Jika bingung berlanjut kunjungi tautan ini https://confluence.atlassian.com/display/BITBUCKET/Fork+a+Repo,+Compare+Code,+and+Create+a+Pull+Request

###Demo
http://zaf.web.id/labs/siakad-unram