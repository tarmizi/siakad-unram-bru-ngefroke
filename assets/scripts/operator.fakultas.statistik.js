/**
 * Created by zaf on 6/28/14.
 */

$(function () {
    $('#chart-dosen').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: 'Data statistik dosen tahun 2010 - 2015'
        },
        xAxis: {
            allowDecimals: false,
            labels: {
                formatter: function() {
                    return this.value; // clean, unformatted number for year
                }
            }
        },
        tooltip: {
            pointFormat: '{series.name} produced <b>{point.y:,.0f}</b><br/>warheads in {point.x}'
        },
        plotOptions: {
            area: {
                pointStart: 1940,
                marker: {
                    enabled: false,
                    symbol: 'circle',
                    radius: 2,
                    states: {
                        hover: {
                            enabled: true
                        }
                    }
                }
            }
        },
        series: [{
            name: 'Dosen Aktif',
            data: [null, null, null, null, null, 6 , 11, 32, 110, 235, 369, 640,
                1005, 1436, 2063, 3057, 4618, 6444, 9822, 15468, 20434, 24126,
                27387, 29459, 31056, 31982, 32040, 31233, 29224, 27342, 26662,
                26956, 27912, 28999, 28965, 27826, 25579, 25722, 24826, 24605,
                24304, 23464, 23708, 24099, 24357, 24237, 24401, 24344, 23586,
                22380, 21004, 17287, 14747, 13076, 12555, 12144, 11009, 10950,
                10871, 10824, 10577, 10527, 10475, 10421, 10358, 10295, 10104 ]
        }]
    });
    $('#chart-mahasiswa').highcharts({
        title: {
            text: 'Data statistik mahasiswa',
            x: -20 //center
        },
        xAxis: {
            categories: ['2010', '2011', '2012', '2013', '2014']
        },
        yAxis: {
            title: {
                text: 'Jumlah mahasiswa'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Aktif',
            data: [18.2, 21.5, 25.2, 26.5, 23.3]
        }, {
            name: 'Cuti',
            data: [17.0, 22.0, 24.8, 8.6, 2.5]
        }, {
            name: 'DO',
            data: [13.5, 17.0, 18.6, 17.9, 14.3]
        }, {
            name: 'Lulus',
            data: [3.9, 4.2, 10.3, 6.6, 4.8]
        }]
    });
    $('#chart-kelulusan').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Data Statistik Kelulusan'
        },
        xAxis: {
            categories: [
                '2010',
                '2011',
                '2012',
                '2013',
                '2014'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Mahasiswa'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Mahasiswa',
            data: [49.9, 71.5, 106.4, 176.0, 135.6]

        }]
    });
    $('#chart-mahasiswa').highcharts({
        title: {
            text: 'Data statistik mahasiswa',
            x: -20 //center
        },
        xAxis: {
            categories: ['2010', '2011', '2012', '2013', '2014']
        },
        yAxis: {
            title: {
                text: 'Jumlah mahasiswa'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Aktif',
            data: [18.2, 21.5, 25.2, 26.5, 23.3]
        }, {
            name: 'Cuti',
            data: [17.0, 22.0, 24.8, 8.6, 2.5]
        }, {
            name: 'DO',
            data: [13.5, 17.0, 18.6, 17.9, 14.3]
        }, {
            name: 'Lulus',
            data: [3.9, 4.2, 10.3, 6.6, 4.8]
        }]
    });
    $('#chart-penerima-beasiswa').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Data Statistik Penerima Beasiswa'
        },
        xAxis: {
            categories: [
                '2010',
                '2011',
                '2012',
                '2013',
                '2014'
            ]
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Jumlah Mahasiswa'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Mahasiswa',
            data: [49.9, 71.5, 106.4, 176.0, 135.6]

        }]
    });
});