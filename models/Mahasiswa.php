<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Model;


class Mahasiswa {

    private $NIM;
    private $kode_prodi;
    private $nama;
    private $tempat_lahir;
    private $tgl_lahir;
    private $jns_kelamin;
    private $kode_agama;
    private $alamat;
    private $kode_provinsi;
    private $thn_masuk;
    private $batas_studi;
    private $status_kuliah;
    private $id_konsentrasi;
    private $sks_diakui;
    private $nomor_ijazah;
    private $nama_wali;
    private $alamat_wali;
    private $kode_pekerjaan_wali;
    private $penghasilan_wali;
    private $tlp_wali;
    private $nomor_transkrip;
    private $status_bayar;
    private $status_masuk;

    /**
     * @param mixed $NIM
     */
    public function setNIM($NIM)
    {
        $this->NIM = $NIM;
    }

    /**
     * @return mixed
     */
    public function getNIM()
    {
        return $this->NIM;
    }

    /**
     * @param mixed $alamat
     */
    public function setAlamat($alamat)
    {
        $this->alamat = $alamat;
    }

    /**
     * @return mixed
     */
    public function getAlamat()
    {
        return $this->alamat;
    }

    /**
     * @param mixed $alamat_wali
     */
    public function setAlamatWali($alamat_wali)
    {
        $this->alamat_wali = $alamat_wali;
    }

    /**
     * @return mixed
     */
    public function getAlamatWali()
    {
        return $this->alamat_wali;
    }

    /**
     * @param mixed $batas_studi
     */
    public function setBatasStudi($batas_studi)
    {
        $this->batas_studi = $batas_studi;
    }

    /**
     * @return mixed
     */
    public function getBatasStudi()
    {
        return $this->batas_studi;
    }

    /**
     * @param mixed $id_konsentrasi
     */
    public function setIdKonsentrasi($id_konsentrasi)
    {
        $this->id_konsentrasi = $id_konsentrasi;
    }

    /**
     * @return mixed
     */
    public function getIdKonsentrasi()
    {
        return $this->id_konsentrasi;
    }

    /**
     * @param mixed $jns_kelamin
     */
    public function setJnsKelamin($jns_kelamin)
    {
        $this->jns_kelamin = $jns_kelamin;
    }

    /**
     * @return mixed
     */
    public function getJnsKelamin()
    {
        return $this->jns_kelamin;
    }

    /**
     * @param mixed $kode_agama
     */
    public function setKodeAgama($kode_agama)
    {
        $this->kode_agama = $kode_agama;
    }

    /**
     * @return mixed
     */
    public function getKodeAgama()
    {
        return $this->kode_agama;
    }

    /**
     * @param mixed $kode_pekerjaan_wali
     */
    public function setKodePekerjaanWali($kode_pekerjaan_wali)
    {
        $this->kode_pekerjaan_wali = $kode_pekerjaan_wali;
    }

    /**
     * @return mixed
     */
    public function getKodePekerjaanWali()
    {
        return $this->kode_pekerjaan_wali;
    }

    /**
     * @param mixed $kode_prodi
     */
    public function setKodeProdi($kode_prodi)
    {
        $this->kode_prodi = $kode_prodi;
    }

    /**
     * @return mixed
     */
    public function getKodeProdi()
    {
        return $this->kode_prodi;
    }

    /**
     * @param mixed $kode_provinsi
     */
    public function setKodeProvinsi($kode_provinsi)
    {
        $this->kode_provinsi = $kode_provinsi;
    }

    /**
     * @return mixed
     */
    public function getKodeProvinsi()
    {
        return $this->kode_provinsi;
    }

    /**
     * @param mixed $nama
     */
    public function setNama($nama)
    {
        $this->nama = $nama;
    }

    /**
     * @return mixed
     */
    public function getNama()
    {
        return $this->nama;
    }

    /**
     * @param mixed $nama_wali
     */
    public function setNamaWali($nama_wali)
    {
        $this->nama_wali = $nama_wali;
    }

    /**
     * @return mixed
     */
    public function getNamaWali()
    {
        return $this->nama_wali;
    }

    /**
     * @param mixed $nomor_ijazah
     */
    public function setNomorIjazah($nomor_ijazah)
    {
        $this->nomor_ijazah = $nomor_ijazah;
    }

    /**
     * @return mixed
     */
    public function getNomorIjazah()
    {
        return $this->nomor_ijazah;
    }

    /**
     * @param mixed $nomor_transkrip
     */
    public function setNomorTranskrip($nomor_transkrip)
    {
        $this->nomor_transkrip = $nomor_transkrip;
    }

    /**
     * @return mixed
     */
    public function getNomorTranskrip()
    {
        return $this->nomor_transkrip;
    }

    /**
     * @param mixed $penghasilan_wali
     */
    public function setPenghasilanWali($penghasilan_wali)
    {
        $this->penghasilan_wali = $penghasilan_wali;
    }

    /**
     * @return mixed
     */
    public function getPenghasilanWali()
    {
        return $this->penghasilan_wali;
    }

    /**
     * @param mixed $sks_diakui
     */
    public function setSksDiakui($sks_diakui)
    {
        $this->sks_diakui = $sks_diakui;
    }

    /**
     * @return mixed
     */
    public function getSksDiakui()
    {
        return $this->sks_diakui;
    }

    /**
     * @param mixed $status_bayar
     */
    public function setStatusBayar($status_bayar)
    {
        $this->status_bayar = $status_bayar;
    }

    /**
     * @return mixed
     */
    public function getStatusBayar()
    {
        return $this->status_bayar;
    }

    /**
     * @param mixed $status_kuliah
     */
    public function setStatusKuliah($status_kuliah)
    {
        $this->status_kuliah = $status_kuliah;
    }

    /**
     * @return mixed
     */
    public function getStatusKuliah()
    {
        return $this->status_kuliah;
    }

    /**
     * @param mixed $status_masuk
     */
    public function setStatusMasuk($status_masuk)
    {
        $this->status_masuk = $status_masuk;
    }

    /**
     * @return mixed
     */
    public function getStatusMasuk()
    {
        return $this->status_masuk;
    }

    /**
     * @param mixed $tempat_lahir
     */
    public function setTempatLahir($tempat_lahir)
    {
        $this->tempat_lahir = $tempat_lahir;
    }

    /**
     * @return mixed
     */
    public function getTempatLahir()
    {
        return $this->tempat_lahir;
    }

    /**
     * @param mixed $tgl_lahir
     */
    public function setTglLahir($tgl_lahir)
    {
        $this->tgl_lahir = $tgl_lahir;
    }

    /**
     * @return mixed
     */
    public function getTglLahir()
    {
        return $this->tgl_lahir;
    }

    /**
     * @param mixed $thn_masuk
     */
    public function setThnMasuk($thn_masuk)
    {
        $this->thn_masuk = $thn_masuk;
    }

    /**
     * @return mixed
     */
    public function getThnMasuk()
    {
        return $this->thn_masuk;
    }

    /**
     * @param mixed $tlp_wali
     */
    public function setTlpWali($tlp_wali)
    {
        $this->tlp_wali = $tlp_wali;
    }

    /**
     * @return mixed
     */
    public function getTlpWali()
    {
        return $this->tlp_wali;
    }

} 