<?php
/**  
 * LRsoft Corp.
 * http://lrsoft.co.id
 *
 * Author : Zaf
 */

namespace SIAKAD\Controller;


class Mahasiswa extends Databases {

    private $field_id;
    private $class_name;
    private $count_query;

    /** @var Mahasiswa $instance */
    private static $instance;

    public static function get_instance() {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    function __construct() {
        parent::_connect();
        $this->field_id = 'NIM';
        $this->class_name = '\SIAKAD\Model\Mahasiswa';
        $this->table_name = 'mahasiswa';
        $this->count_query = 'SELECT * FROM ' . $this->table_name;
    }

    /**
     * @param $id
     * @param string $by
     * @return \SIAKAD\Model\Mahasiswa
     */
    function _get( $id, $by = '' ) {
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE `' . ( empty( $by ) ? $this->field_id : $id ) . '` = "' . $id . '"';
        return ( $return = mysql_fetch_object(
            mysql_query( $query ), $this->class_name
        ) ) ? $return : new $this->class_name;
    }

    function _gets( $args = array() ) {

        $return = array();

        $default_args = array(
            'kode_prodi'                => -1,
            'nama'                      => '',
            'tempat_lahir'              => '',
            'tanggal_lahir_start_date'  => '',
            'tanggal_lahir_end_date'    => '',
            'jenis_kelamin'             => -1,
            'kode_agama'                => -1,
            'exclude'                   => array(),
            'conditions'                => '',
            'orderby'                   => $this->field_id,
            'order'                     => 'DESC',
            'number'                    => 10,
            'offset'                    => 0
        );

        $list_args = sync_default_params( $default_args, $args );
        $query = 'SELECT * FROM ' . $this->table_name . ' WHERE 1';

        /**
         * kode prodi
         */
        if( $list_args[ 'kode_prodi' ] >= 0 )
            $query .= ' AND ' . $this->table_name . '.kode_prodi = ' . $list_args[ 'kode_prodi' ];

        /**
         * Range tanggal lahir
         */
        if( !empty( $list_args[ 'tanggal_lahir_start_date' ] ) )
            $query .= ' AND ' . $this->table_name . '.tanggal_lahir_start_date <= "' . $list_args[ 'tanggal_lahir_start_date' ] . '"';

        if( !empty( $list_args[ 'tanggal_lahir_end_date' ] ) )
            $query .= ' AND ' . $this->table_name . '.tanggal_lahir_end_date >= "' . $list_args[ 'tanggal_lahir_end_date' ] . '"';

        /**
         * exclude
         */
        if( !empty( $list_args[ 'exclude' ] ) ) {

            foreach( $list_args[ 'exclude' ] as $ex )
                $query .= ' AND `NIM` <> ' . $ex;

        }

        /**
         * untuk custom query pada conditions
         */
        if( !empty( $list_args[ 'conditions' ] ) ) {
            foreach( $list_args[ 'conditions' ] as $conditions )
                $query .= ' AND ' . $list_args . '.' . $conditions[ 'field' ] . ' ' . $conditions[ 'operator' ] . ' ' . $conditions[ 'comparison' ];
        }

        $this->count_query = $query;

        /**
         * orderby dan jenis order
         */
        $query .= ' ORDER BY `' . $list_args[ 'orderby' ] . '` ' . $list_args[ 'order' ];

        /**
         * limit
         */
        if( $list_args[ 'number' ] >= 0 )
            $query .= ' LIMIT ' . $list_args[ 'offset' ] . ', ' . $list_args[ 'number' ];

        $resource = mysql_query( $query );

        //echo $query . ' : ' . mysql_error();

        while( $row = mysql_fetch_object( $resource, $this->class_name ) )
            $return[] = $row;

        return $return;

    }

}